-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2018 at 12:07 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projects`
--

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `Name` varchar(255) NOT NULL,
  `id` int(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `guide` varchar(50) NOT NULL,
  `year` year(4) NOT NULL,
  `PL` varchar(20) NOT NULL,
  `domain` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `review` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`Name`, `id`, `author`, `guide`, `year`, `PL`, `domain`, `status`, `review`) VALUES
('Library Management', 1, 'Sunil Kumar', 'Srikanth H R', 2001, 'HTML, CSS, JavaScrip', 'Web', 0, 1),
('Timetable Generation', 2, 'Vasudev', 'Sathyam Vellal', 2010, 'Python', 'GUI based Desktop Application', 0, 1),
('Management System for Academic Projects', 3, 'Somashekhar', 'Phalachandra', 2008, 'HTML5, CSS3, Javascr', 'Web', 1, 1),
('Faculty Adviser Automation', 4, 'Shivaraj', 'N S Kumar', 2009, 'Java', 'GUI based Desktop Application', 1, 1),
('Sparse Matrix', 5, 'Niranjan', 'H B Mahesh', 2011, 'C', 'Data-Structures', 0, 1),
('14', 9, 'xyz', 'abc', 2018, 'php', 'web', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `name` varchar(10) NOT NULL,
  `id` int(11) NOT NULL,
  `ta` varchar(200) NOT NULL,
  `grant1` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`name`, `id`, `ta`, `grant1`) VALUES
('1pi10cs116', 1, 'hello', 1),
('kavi_24', 3, 'BBJK', 1),
('kavi_24', 9, 'for my projet', 1),
('madhu_29', 3, 'hellooooooooo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `usn` varchar(20) NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `usertype` varchar(30) NOT NULL,
  `DOB` date NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `phone` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `usn`, `passwd`, `email`, `usertype`, `DOB`, `gender`, `phone`) VALUES
(1, 'kavi', 'kavi_24', '5f4dcc3b5aa765d61d8327deb882cf99', 'kavi@gmail.com', 'admin', '2018-02-02', 0, '1234567890'),
(2, 'madhavi', 'madhu_29', 'c5693debdf7717b3508df641c60e1b0e', 'madhu@gmail.com', 'user', '1995-07-29', 0, '987654321');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`name`,`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
